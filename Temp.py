import random
import math
import pygame
from pygame.locals import *


SCREENWIDTH = 800
SCREENHEIGHT = 600
PLAYER_SPEED = 0.6
ENEMY_SPEED = [0.3,0.6,0.4]
BULLET_SPEED = 1
SCORE =0
No_Of_Enemy=10
GAME_SOUNDS = {}

# Set a SCREEN Window
SCREEN = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))

# Character function is used  to blit aplayer or enemey image on the screen to a particular location
def character(chr,x,y):
    if x>= SCREENWIDTH-64:
        x= SCREENWIDTH-64
    if x<= 0:
        x = 0
    SCREEN.blit(chr, (x,y))

# Player define
playerX = 370
playerY = 480
playerX_change=0

# def player(PLAYER,x,y):
#     if x>= SCREENWIDTH-64:
#         x= SCREENWIDTH-64
#     if x<= 0:
#         x = 0
#     SCREEN.blit(PLAYER, (x,y))

# Enemy define
# Path of tbe enemy image in a list
enemyImg=['images/enemy.png','images/enemy2.png','images/enemy3.png','images/enemy4.png','images/enemy5.png']
enemyX =  []
enemyY =  []
enemyX_change= []
enemyY_change= []
enemy_speed=[]
for i in range(No_Of_Enemy):
    enemyX.append(random.randint(0,736))
    enemyY.append(random.randint(10,120))
    enemy_speed.append(random.choice(ENEMY_SPEED))
    enemyX_change.append(enemy_speed[i])
    enemyY_change.append(40)

# Enemy function for define a location of enemy
# def enemy(ENEMY,x,y):
#     if x>= SCREENWIDTH-64:
#         x= SCREENWIDTH-64
#     if x<= 0:
#         x = 0
#     SCREEN.blit(ENEMY, (x,y))

# Bullet define
bulletX = 0
bulletY = 480
bulletY_change=BULLET_SPEED
is_bulletFire=False

# fire bullet function
def fire_bullet(x,y):
    global is_bulletFire
    is_bulletFire=True
    SCREEN.blit(BULLET,(x + 20,y + 10))

# Is collision occur function
def is_collision(bx,by,ex,ey):
    distance= math.sqrt((math.pow((ex-bx),2)) + (math.pow((ey-by),2)))
    if distance <=32:
        pygame.mixer.Sound.play(GAME_SOUNDS['die'])
        return True
    else:
        return False

# Score function to update a score on the screen
def displayScore(font):
    score=font.render(f"SCORE: {SCORE}", True, (255,255,255))
    SCREEN.blit(score,(10,10))
    if SCORE %50 ==0 and SCORE > 0:
         pygame.mixer.Sound.play(GAME_SOUNDS['point'])


# Game Over to Print GameOver on the screen

def  showGameover(font):
    over=font.render("GAME OVER", True, (200,50,10))
    SCREEN.blit(over,(190,250))


if __name__ == "__main__":

    # This will be the main point from where our game will start because it is the main function
    pygame.init()  # Initialize all pygame's modules

    # convert_alpha is used for render the images      
    icon = pygame.image.load('images/alien.png').convert_alpha() 
    BACKGROUND = pygame.image.load('images/Background.png').convert()
    font = pygame.font.Font('Font/magic_hat.ttf', 20)
    gameOver = pygame.font.Font('Font/magic_hat.ttf', 70)
    PLAYER = pygame.image.load('images/spaceship.png').convert_alpha()
    BULLET = pygame.image.load('images/bullet.png').convert_alpha()
    ENEMY=[]
    for i in range(No_Of_Enemy):
        ENEMY.append(pygame.image.load(random.choice(enemyImg)).convert_alpha())

    # Game sound
    GAME_SOUNDS['die'] = pygame.mixer.Sound('sound/explosion.wav')
    GAME_SOUNDS['hit'] = pygame.mixer.Sound('sound/laser.wav')
    GAME_SOUNDS['point'] = pygame.mixer.Sound('sound/point.wav')
     
    # Set a  Game Caption and Icon On Game Window
    pygame.display.set_caption('Space War')
    pygame.display.set_icon(icon)
   
#    Sound FOr background sound
    pygame.mixer.music.load('sound/background.wav')
    pygame.mixer.music.play(-1)

    # Game Loop
    running = True
    while running:
    
        # Add a background in game window 
        # SCREEN.fill((0, 0, 0)) 
        SCREEN.blit(BACKGROUND,(0,0))

        # for Quit the game
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        # For Keystroke in the game
            if event.type == pygame.KEYDOWN:
                # for handiling the Left arrow key
                if event.key== pygame.K_LEFT:
                    playerX_change=-PLAYER_SPEED
                # for handiling the Right arrow key
                if event.key== pygame.K_RIGHT:
                    playerX_change=PLAYER_SPEED
                # for handiling the spacebar for fire a bullet
                if event.key== pygame.K_SPACE and is_bulletFire == False:
                    bulletX=playerX
                    fire_bullet(bulletX,bulletY)  
                    pygame.mixer.Sound.play(GAME_SOUNDS['hit'])
                    
                    

            # for handiling LEFT and RIGHT arrow realease
            if event.type == pygame.KEYUP:
                if event.key== pygame.K_LEFT or event.key== pygame.K_RIGHT:
                    playerX_change=0

        for i in range(No_Of_Enemy):
        # Enemy Movement
            enemyX[i]+=enemyX_change[i];
            if enemyX[i]<=0:
                enemyX_change[i]=enemy_speed[i]
                enemyY[i] += enemyY_change[i]
            elif enemyX[i] >= SCREENWIDTH-64:
                enemyX_change[i]=-enemy_speed[i]
                enemyY[i] += enemyY_change[i]

            # Collision OCCur or not
            if is_collision(bulletX,bulletY,enemyX[i],enemyY[i]):
                bulletY=480
                SCORE+=1
                is_bulletFire=False
                ENEMY[i]=pygame.image.load(random.choice(enemyImg)).convert_alpha()
                enemyX[i] = random.randint(0,736)
                enemyY[i] = random.randint(10,120)

            #  gameOver condition   
            if enemyY[i] >=416:
                showGameover(gameOver)
                for i in range(No_Of_Enemy):
                    enemyY[i]=1000
                break;
            character(ENEMY[i],enemyX[i],enemyY[i])
 

       #  Bullet MOVEMENT
        if bulletY<=-16:
            is_bulletFire=False
            bulletY = 480
        if is_bulletFire == True:
            fire_bullet(bulletX,bulletY)
            bulletY+=-bulletY_change
        elif is_bulletFire ==False:
            bulletY=playerY

        # Calling display score function
        displayScore(font)

        # Update a value of playerX and Call player Function
        playerX+=playerX_change
        character(PLAYER,playerX,playerY)

       # Call enemy Function
       

        

    # Update all thing in game window
        pygame.display.update()
